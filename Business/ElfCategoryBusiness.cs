﻿using Fr.EQL.AI110.ChristmasFactory.DataAccess;
using Fr.EQL.AI110.ChristmasFactory.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.ChristmasFactory.Business
{
    public class ElfCategoryBusiness
    {
        public List<ElfCategory> GetElfCategories()
        {
            ElfCategoryDAO dao = new ElfCategoryDAO();
            return dao.GetElfCategories();
            
        }
    }
}
