﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Fr.EQL.AI110.ChristmasFactory.Entities;
using Fr.EQL.AI110.ChristmasFactory.DataAccess;

namespace Fr.EQL.AI110.ChristmasFactory.Business
{
    public class ToyBusiness
    {
        public void SaveToy(Toy toy)
        {
            //Appliquer les règles de gestion
            if (toy.Price < Toy.MIN_PRICE|| toy.Price > Toy.MAX_PRICE)
            {
                throw new Exception("Le prix doit être compris entre " + Toy.MIN_PRICE + " et " + Toy.MAX_PRICE +".");
            }

            ToyDAO dao = new ToyDAO();
           
            //Envoyer au DAO
            try
            { 
                if (toy.Id == 0)
                {
                    dao.Insert(toy);
                }
                else
                {
                    dao.Update(toy);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Erreur lors de l'insertion d'un toy! " + ex.Message);
            }
        }

        public Toy GetToyById(int id)
        {
            ToyDAO dao = new ToyDAO();
            return dao.GetById(id);

        }
        public List<ToyDetails> SearchToys(int idResponsable, string name, int min, int max)
        {
            ToyDAO dao = new ToyDAO();
            return dao.GetByMultiCriteria(idResponsable, name, min, max);
        }
        public List<Toy> GetToys()
        {
            ToyDAO dao = new ToyDAO();
            return dao.GetAll();
        }

        public void DeleteToy(int id)
        {
            ToyDAO dao = new ToyDAO();
            dao.Delete(id);
        }

        //public void UpdateToy(Toy toy)
        //{
        //    ToyDAO dao = new ToyDAO();
        //    dao.Update(toy);
        //}
    }
}
