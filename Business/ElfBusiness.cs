﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Fr.EQL.AI110.ChristmasFactory.Entities;
using Fr.EQL.AI110.ChristmasFactory.DataAccess;

namespace Fr.EQL.AI110.ChristmasFactory.Business
{
    public class ElfBusiness
    {
        public const int NAME_MIN_NAME = 3;
        public void SaveElf(Elf elf)
        {
            //Appliquer les règles de gestion
            if (elf.Name.Length < Elf.NAME_MIN_NAME)
            {
                throw new Exception("Création impossible: le nom doit faire au moins 3 caractères.");
            }

            //Envoyer au DAO
            ElfDAO dao = new ElfDAO();
             try
            {
                if (elf.Id == 0)
                {
                    dao.Insert(elf);
                }
                else
                {
                    dao.Update(elf);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Erreur lors de l'insertion d'un elf! " + ex.Message);
            }
        }
      
        public List<ElfDetails> SearchElves(string name, bool? isAvailable, string picture, int idCategory)
        {
            ElfDAO dao = new ElfDAO();
            return dao.GetElfByMultiCriteria(name, isAvailable, picture,idCategory);
        }

        public Elf GetElfById(int id)
        {
            //return new ElfDAO().GetById(id);
            ElfDAO dao = new ElfDAO();
            return dao.GetById(id);
            
        }

        public List<Elf> GetElves()
        {
            ElfDAO dao = new ElfDAO();
            return dao.GetAll();
        }

        public void DeleteElf(int id)
        {
            ElfDAO dao = new ElfDAO();
            dao.Delete(id);
        }

    }
}
