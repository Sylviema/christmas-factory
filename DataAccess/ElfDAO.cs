﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MySql.Data.MySqlClient;
using System.Data.Common;
using Fr.EQL.AI110.ChristmasFactory.Entities;

namespace Fr.EQL.AI110.ChristmasFactory.DataAccess
{
    // Pour l'héritage
    // java extends
    // C# :
    public class ElfDAO : DAO
    {
        public void Insert(Elf e)
        {
            //créer un object connexion
            DbConnection cnx = new MySqlConnection();
            //1 - paramétrer la connexion
            cnx.ConnectionString = CNX_STR;
            //2 - paramétrer la commande
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"INSERT INTO elf 
                                            (name,is_available,picture,id_category)
                                            VALUES 
                                            (@name,@isAvailable,@picture,@idCategory)";

            cmd.Parameters.Add(new MySqlParameter("name",e.Name));
            cmd.Parameters.Add(new MySqlParameter("isAvailable", e.IsAvailable));
            cmd.Parameters.Add(new MySqlParameter("picture", e.Picture));
            cmd.Parameters.Add(new MySqlParameter("idCategory", e.IdCategory));

            try 
            { 
            //3 - ouvrir la connexion
            cnx.Open();
            //4 - exécuter la commande
            int nbLignes = cmd.ExecuteNonQuery();
            //5 - Traiter le résultat
            //Console.WriteLine(nbLignes + " lignes insérées.");
            //6 - fermer la connexion

            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de l'insertion d'un elf! " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
        }
        
        public List<Elf> GetAll()
        {
            //          Java        C#
            //interface List        IList
            //classe    ArrayList   List
            List<Elf> result = new List<Elf>();
           
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = "SELECT * FROM elf";
            try
            {
            cnx.Open();
            DbDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                Elf elf = DataReaderToEntity(dr);
                result.Add(elf);
            }
            cnx.Close();
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de l'affichage" + ex.Message);
            }

            return result;
        }

        public Elf GetById(int id)
        {
            Elf result = null;

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM elf
                                         WHERE id=@id";

            cmd.Parameters.Add(new MySqlParameter("id", id));
            try
            {
                cnx.Open();
            DbDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                result = DataReaderToEntity(dr);

            }
            cnx.Close();
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de l'affichage" + ex.Message);
            }
            return result;
        }
        public List<ElfDetails> GetElfByMultiCriteria(string name, bool? isAvailable, string picture, int idCategory)
        {
            List<ElfDetails> result = new List<ElfDetails>();

            name = "%" + name + "%"; 
            picture = "%" + picture + "%";

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT e.*,c.name 'category' 
                                FROM elf e LEFT JOIN elfcategory c ON e.id_category = c.id
                                WHERE (id_category = @idCategory OR @idCategory = 0)
                                AND (e.name LIKE @name)
                                AND (e.picture LIKE @picture)
                                AND (e.is_available = @isAvailable OR @isAvailable is null)
                                ";
                                

            cmd.Parameters.Add(new MySqlParameter("name", name));
            cmd.Parameters.Add(new MySqlParameter("isAvailable", isAvailable));
            cmd.Parameters.Add(new MySqlParameter("picture", picture));
            cmd.Parameters.Add(new MySqlParameter("idCategory", idCategory));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Elf elf = DataReaderToEntity(dr);

                    ElfDetails detail = new ElfDetails();
                    detail.Name = elf.Name;
                    detail.Picture = elf.Picture;
                    detail.IsAvailable = elf.IsAvailable;
                    detail.Id = elf.Id;
                    if (!dr.IsDBNull(dr.GetOrdinal("id_category")))
                    {
                         detail.IdCategory = elf.IdCategory;
                    }
                    if (!dr.IsDBNull(dr.GetOrdinal("category")))
                    {
                        detail.CategoryName = dr.GetString(dr.GetOrdinal("category"));
                    }
                    result.Add(detail);
                }

            }
            catch(MySqlException ex)
            {
                throw new Exception("Erreur lors de l'affichage" + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }

        //Update (par défaut : Update All)
        //On peut avoir des UpdateName(int id), UpdateIsAvailable(int id), UpdatePicture(int id)
        public void Update(Elf e)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"UPDATE 
                                      elf 
                                      SET name = @name,
                                          is_available = @isAvailable,
                                          picture = @picture,
                                          id_category = @idCategory
                                      WHERE 
                                          id = @id";

            cmd.Parameters.Add(new MySqlParameter("name", e.Name));
            cmd.Parameters.Add(new MySqlParameter("isAvailable", e.IsAvailable));
            cmd.Parameters.Add(new MySqlParameter("picture", e.Picture));
            cmd.Parameters.Add(new MySqlParameter("id", e.Id));
            cmd.Parameters.Add(new MySqlParameter("idCategory", e.IdCategory));

            try
            {
            cnx.Open();
            int nbLignes = cmd.ExecuteNonQuery();
            Console.WriteLine(nbLignes + " lignes updatées.");
            cnx.Close();
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la mise à jour" + ex.Message);
            }
        }

        public void Delete(int id)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"DELETE FROM elf 
                                       WHERE 
                                            id = @id";
            cmd.Parameters.Add(new MySqlParameter("id", id));
            try
            {
            cnx.Open();
            cmd.ExecuteNonQuery();
            cnx.Close();
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la suppression" + ex.Message);
            }
        }

        private Elf DataReaderToEntity(DbDataReader dr)
        {
            Elf elf= new Elf();

            elf.Id = dr.GetInt32(dr.GetOrdinal("id"));
            elf.Name = dr.GetString(dr.GetOrdinal("name")); // (string) dr["name"]
            elf.IsAvailable = dr.GetBoolean(dr.GetOrdinal("is_available")); // (bool) dr["is_available"]
            if (!dr.IsDBNull(dr.GetOrdinal("picture")))
            {
                elf.Picture = dr.GetString(dr.GetOrdinal("picture")); // (picture) dr["picture"]
            }
            if (!dr.IsDBNull(dr.GetOrdinal("id_category")))
            {
                elf.IdCategory = dr.GetInt32(dr.GetOrdinal("id_category"));
            }
            

            return elf;
        }

    }
}
