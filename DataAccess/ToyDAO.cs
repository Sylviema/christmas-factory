﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MySql.Data.MySqlClient;
using System.Data.Common;
using Fr.EQL.AI110.ChristmasFactory.Entities;

namespace Fr.EQL.AI110.ChristmasFactory.DataAccess
{
    public class ToyDAO : DAO
    {
        public void Insert(Toy t)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"INSERT INTO toy
                                       (name,description,price,id_responsable)
                                       VALUES
                                       (@name,@description,@price,@idResponsable)";

            cmd.Parameters.Add(new MySqlParameter("name",t.Name));
            cmd.Parameters.Add(new MySqlParameter("description", t.Description));
            cmd.Parameters.Add(new MySqlParameter("price", t.Price));
            cmd.Parameters.Add(new MySqlParameter("idResponsable", t.IdResponsable));
            try
            {
            cnx.Open();
            int nbreLignes = cmd.ExecuteNonQuery();
            Console.WriteLine(nbreLignes+" lignes insérées.");
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de l'insertion d'un jouet! " + ex.Message);
            }
            finally
            {
            cnx.Close();
            }
        }

        public List<Toy> GetAll()
        {
            List<Toy> result = new List<Toy>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM toy";
            try
            {
            cnx.Open();
            DbDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                Toy toy = DataReaderToEntity(dr);
                result.Add(toy);
            }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de l'affichage" + ex.Message);
            }
            finally
            {
            cnx.Close();
            }
            return result;
        }
        public Toy GetById(int id)
        {
            Toy toy = null;

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM toy
                                         WHERE id=@id";

            cmd.Parameters.Add(new MySqlParameter("id", id));
            try
            {
            cnx.Open();
            DbDataReader dr = cmd.ExecuteReader();
          
            if (dr.Read())
            {
                toy = DataReaderToEntity(dr);
            }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de l'affichage" + ex.Message);
            }
            finally
            { 
                cnx.Close(); 
            }

            return toy;
        }

        public List<ToyDetails> GetByMultiCriteria(int idResponsable, string name, int min, int max)
        {
            List<ToyDetails> result = new List<ToyDetails>();

            name = "%" + name + "%";

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT t.*, e.name 'responsable' 
                                         FROM toy t LEFT JOIN elf e ON t.id_responsable = e.id
                                         WHERE (id_responsable = @idResponsable OR @idResponsable = 0) 
                                                AND (t.name LIKE @name) 
                                                AND (price >= @minPrice OR @minPrice = 0) 
                                                AND (price <= @maxPrice OR @maxPrice = 0)";
            cmd.Parameters.Add(new MySqlParameter ("idResponsable", idResponsable));
            cmd.Parameters.Add(new MySqlParameter ("name", name));
            cmd.Parameters.Add(new MySqlParameter ("minPrice", min));
            cmd.Parameters.Add(new MySqlParameter ("maxPrice", max));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {

                    Toy toy = DataReaderToEntity(dr);

                    ToyDetails toyDetails = new ToyDetails();

                    toyDetails.Id = toy.Id;
                    toyDetails.Name = toy.Name;
                    toyDetails.Description = toy.Description;
                    toyDetails.Price = toy.Price;
                    toyDetails.IdResponsable = toy.IdResponsable;

                    if (!dr.IsDBNull(dr.GetOrdinal("responsable")))
                    {
                        toyDetails.RespName = dr.GetString(dr.GetOrdinal("responsable"));
                    }

                    result.Add(toyDetails);

                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de l'affichage" +ex.Message);
            }
            finally
            {
                cnx.Close();
            }

            return result;
        }
        public void Update(Toy t)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"UPDATE toy
                                       SET name = @name,
                                           description = @description,
                                           price = @price,
                                           id_responsable = @idResponsable
                                       WHERE id = @id";

            cmd.Parameters.Add(new MySqlParameter("name",t.Name));
            cmd.Parameters.Add(new MySqlParameter("description",t.Description));
            cmd.Parameters.Add(new MySqlParameter("price",t.Price));
            cmd.Parameters.Add(new MySqlParameter("idResponsable",t.IdResponsable));
            cmd.Parameters.Add(new MySqlParameter("id",t.Id));

            try 
            { 
            cnx.Open();
            int nbreLignes = cmd.ExecuteNonQuery();
            Console.WriteLine(nbreLignes + " lignes updatées.");
             }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la mise à jour" + ex.Message);
             }
            finally
            {
            cnx.Close();
            }
        }
        public void Delete(int id)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"DELETE FROM toy
                                            WHERE id=@id";

            cmd.Parameters.Add(new MySqlParameter("id",id));
            try { 
            cnx.Open();
            int nbreLignes = cmd.ExecuteNonQuery ();
            Console.WriteLine(nbreLignes + " lignes effacées.");
            cnx.Close ();
               }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la suppression" + ex.Message);
            }

        }

        private Toy DataReaderToEntity(DbDataReader dr)
        {
            Toy toy = new Toy();

            toy.Id = dr.GetInt32(dr.GetOrdinal("id"));
            toy.Name = dr.GetString(dr.GetOrdinal("name"));
            toy.Description = dr.GetString(dr.GetOrdinal("description"));
            toy.Price = dr.GetFloat(dr.GetOrdinal("price"));
            if (!dr.IsDBNull(dr.GetOrdinal("id_responsable")))
            {
                toy.IdResponsable = dr.GetInt32(dr.GetOrdinal("id_responsable"));
            }

            return toy;
        }


    }
 }
