﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MySql.Data.MySqlClient;
using System.Data.Common;
using Fr.EQL.AI110.ChristmasFactory.Entities;

namespace Fr.EQL.AI110.ChristmasFactory.DataAccess
{
    public class ElfCategoryDAO : DAO
    {

        public List<ElfCategory> GetElfCategories()
        {
            List<ElfCategory> result = new List<ElfCategory>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM elfcategory";

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    ElfCategory category = new ElfCategory();
                    category.Id = dr.GetInt32(dr.GetOrdinal("id"));
                    category.CategoryName = dr.GetString(dr.GetOrdinal("name"));

                    result.Add(category);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Erreur lors de l'affichage" + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }
    }
}
