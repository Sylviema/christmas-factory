﻿using System.ComponentModel.DataAnnotations;

namespace Fr.EQL.AI110.ChristmasFactory.Entities
{
    public class Elf
    {
        public const int NAME_MIN_NAME = 3;
        public const string NAME_ERROR_MSG = "Le nom doit comporter au moins 3 caractères";
        #region ATTRIBUTES
        public int Id { get; set; }

        [Required(ErrorMessage = "Le nom est obligatoire")]
        [MinLength(NAME_MIN_NAME,ErrorMessage = NAME_ERROR_MSG)]
        public string Name { get; set; }
        public Boolean IsAvailable { get; set; }
        public string Picture { get; set; }
        public int IdCategory { get; set; }
        #endregion

        #region CONSTRUCTORS
        public Elf()
        {
        }

        public Elf(int id)
        {
            Id = id;
        }

        public Elf(int id, string name, bool isAvailable, string picture)
        {
            Id = id;
            Name = name;
            IsAvailable = isAvailable;
            Picture = picture;
        }

        public Elf(string name, bool isAvailable, string picture)
        {
            Name = name;
            IsAvailable = isAvailable;
            Picture = picture;
        }


        #endregion

        #region METHODS
        public override string? ToString()
        {
            string result = "";
            if (IsAvailable)
            {
                result = "Oui";
            }
            else
            {
                result = "Non";
            }
            return "id : " +this.Id + " - Nom : " + this.Name + " - Disponible : " + result;
        }
        #endregion
    }
}