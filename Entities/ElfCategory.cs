﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.ChristmasFactory.Entities
{
    public class ElfCategory
    {
        public int Id { get; set; }
        public string CategoryName { get; set; }
    }
}
