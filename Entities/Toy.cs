﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.ChristmasFactory.Entities
{
    public class Toy
    {
        public const int MIN_PRICE = 0;
        public const int MAX_PRICE = 1000;

        #region ATTRIBUTES
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        [Range(MIN_PRICE,MAX_PRICE, ErrorMessage = "Le prix doit etre compris entre 0 et 1000€")]
        public float Price { get; set; }
        //int? : nullable int
        public int? IdResponsable { get; set; }
        #endregion

        #region CONSTRUCTORS
        public Toy()
        {
            IdResponsable = null;
        }

        public Toy(int id, string name, string description, float price, int idResponsable)
        {
            Id = id;
            Name = name;
            Description = description;
            Price = price;
            IdResponsable = idResponsable;
        }

        public override string? ToString()
        {
            return "id : " + this.Id + " - Nom : " + this.Name + " - Description : " + this.Description + " - Prix : " + this.Price + " - Id du Responsable : "+IdResponsable;
        }
        #endregion


    }

}
