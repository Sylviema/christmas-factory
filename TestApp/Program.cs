﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Fr.EQL.AI110.ChristmasFactory.Entities;
using Fr.EQL.AI110.ChristmasFactory.Business;

namespace Fr.EQL.AI110.ChritsmasFactory.TestApp

{
    public class Program
    {

        public static void Main(string[] args)
        {
            //TestInsertion();
            TestMiseAJour();
            //TestSuppression();
            //AfficherTous();
            //AfficherUn();
            //TestAjoutToy();
            //SuppressionToy();
            
        }

        public static void TestMiseAJour()
        {
            Console.WriteLine("id à modifier ?");
            int id = int.Parse(Console.ReadLine());
            Console.WriteLine("Nouveau nom : ");
            string nom = Console.ReadLine();
            Console.WriteLine("Nouvelle description : ");
            string description = Console.ReadLine();
            Console.WriteLine("Nouveau prix ?");
            float price = float.Parse(Console.ReadLine());

            Toy toy = new Toy(id, nom, description, price, 2);

            ToyBusiness bu = new ToyBusiness();
            try
            {
                bu.SaveToy(toy);
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex.Message);
                Console.ResetColor();
            }
        }

        public static void TestSuppression()
        {
            Console.WriteLine("id à supprimer ?");
            int id = int.Parse(Console.ReadLine());
            ElfBusiness bu = new ElfBusiness();
            try
            {
                bu.DeleteElf(id);
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex.Message);
                Console.ResetColor();
            }
        }
        public static void SuppressionToy()
        {
            Console.WriteLine("id à supprimer ?");
            int id = int.Parse(Console.ReadLine());
            ToyBusiness bu = new ToyBusiness();
            try
            {
                bu.DeleteToy(id);
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex.Message);
                Console.ResetColor();
            }
        }

        public static void TestAjoutToy()
        {
            Console.Write("Nom du jouet ? ");
            string nom = Console.ReadLine();
            Console.Write("Description du jouet ? : ");
            string description = Console.ReadLine();
            Console.Write("Prix du jouet ? : ");
            float price = float.Parse(Console.ReadLine());
            Toy toy = new Toy(0, nom, description,price,6);
            ToyBusiness bu = new ToyBusiness();
            try
            {
                bu.SaveToy(toy);
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex.Message);
                Console.ResetColor();
            }
        }

        public static void TestInsertion()
        {
            Console.Write("Nom de l'elfe ? ");
            string nom = Console.ReadLine();
            Elf e = new Elf(0, nom, true, "photo.jpg");
            ElfBusiness bu = new ElfBusiness();
            try
            {
                bu.SaveElf(e);
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex.Message);
                Console.ResetColor();
            }
        }

        public static void AfficherUn()
        {

            Console.Write("Choisissez l'id de l'elf : ");
            int id = int.Parse(Console.ReadLine());
            ElfBusiness bu = new ElfBusiness();
            try
            {
                Elf elf = bu.GetElfById(id);
                Console.WriteLine(elf);
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex.Message);
                Console.ResetColor();
            }
        }

    }
}

        /*
        public static void AfficherTous()
        {
            ElfDAO dao = new ElfDAO();
            List<Elf> listElves = dao.GetAll();
            try
            {
                foreach (Elf elf in listElves)
            {
                Console.WriteLine(elf);
            }
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex.Message);
                Console.ResetColor();
            }

        }
        public static void AfficherUn()
        {
            Console.Write("Choisissez l'id de l'elf : ");
            int id = int.Parse(Console.ReadLine());
            ElfDAO dao = new ElfDAO();
            try
            {
            Elf elf = dao.GetById(id);
            Console.WriteLine(elf);
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex.Message);
                Console.ResetColor();
            }
            // if (elf != null)
            // {
            // Console.WriteLine(elf.Name);
            // Console.WriteLine(elf.IsAvailable? "dispo" : "pas dispo");
            // Console.WriteLine(elf.Piscture);
            // }
        }
        public static void TestInsertion()
        {
            Console.Write("Nom de l'elfe ? ");
            string nom = Console.ReadLine();
            Elf e = new Elf(0, nom, true, "dobbie.jpg");
            ElfDAO dao = new ElfDAO();
            try
            {
                dao.Insert(e);
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex.Message);
                Console.ResetColor();
            }

        
        }
        public static void TestMiseAJour()
        {
            Console.WriteLine("id à modifier ?");
            int id = int.Parse(Console.ReadLine());
            Console.WriteLine("Nouveau nom : ");
            string nom = Console.ReadLine();

            Elf e = new Elf(id, nom, true, "plop.jpg");

            ElfDAO dao = new ElfDAO();
            try
            {
            dao.Update(e);
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex.Message);
                Console.ResetColor();
            }
}
        
    }
        */
    

