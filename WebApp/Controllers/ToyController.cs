﻿using Fr.EQL.AI110.ChristmasFactory.Business;
using Fr.EQL.AI110.ChristmasFactory.Entities;
using Fr.EQL.AI110.ChristmasFactory.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace Fr.EQL.AI110.ChristmasFactory.Web.Controllers
{
    public class ToyController : Controller
    {
        private void ElfBu()
        {
            ElfBusiness bu = new ElfBusiness();
            ViewBag.ElfList = bu.GetElves();
        }
        public void ElfName(int id)
        {
            ElfBusiness bu = new ElfBusiness();
            ViewBag.ElfName = bu.GetElfById(id).Name;
        }
        private IActionResult EditToy(string rubrique, Toy toy)
        {
            ViewBag.Rubrique = rubrique;
            return View("Edit", toy);
        }

        [HttpGet]
        public IActionResult Index()
        {
            ToySearchViewModel model = new ToySearchViewModel();

            ToyBusiness toyBu = new ToyBusiness();
            model.Toys = toyBu.SearchToys(0,"",0,0);
            
            ElfBusiness elfBu = new ElfBusiness();
            model.Elves = elfBu.GetElves();

            return View(model);
        }

        [HttpPost]
        public IActionResult Index(ToySearchViewModel model)
        {
           
            ToyBusiness toyBu = new ToyBusiness();
            model.Toys = toyBu.SearchToys(model.IdResponsable, model.ToyName, model.MinPrice, model.MaxPrice);

            ElfBusiness elfBu = new ElfBusiness();
            model.Elves = elfBu.GetElves();

            return View(model);

        }
        [HttpGet]
        public IActionResult Create()
        {
            ElfBu();
            return EditToy("Création d'un jouet",null);
        }

        [HttpPost]
        public IActionResult Create(Toy toy)
        {
            ElfBu();
            if (ModelState.IsValid)
            {
                ToyBusiness bu = new ToyBusiness();
                bu.SaveToy(toy);
                return RedirectToAction("Index");
            } 
            else
            {
                return EditToy("Création d'un jouet", null);
            }
        }
        [HttpGet]
        public IActionResult Update(int id)
        {
            ElfBu();
            ToyBusiness bu = new ToyBusiness();
            Toy toy = bu.GetToyById(id);
            return EditToy("Modification d'un jouet", toy);
        }
        [HttpPost]
        public IActionResult Update(Toy toy)
        {
            ElfBu();
            if (ModelState.IsValid)
            {
                try
                {
                ToyBusiness bu = new ToyBusiness();
                bu.SaveToy(toy);
                return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ViewBag.ErrorMessage = ex.Message;
                    return EditToy("Modification d'un jouet", toy);
                }
            }
            else
            {
                return EditToy("Modification d'un jouet", toy);
            }
        }

        public IActionResult Detail(int id)
        {
            ToyBusiness bu = new ToyBusiness();
            Toy toy = bu.GetToyById(id);
            
           if ( toy.IdResponsable != null)
            {
                ElfName((int)toy.IdResponsable);
            }
            return View(toy);
        }
        [HttpPost]
        public IActionResult Delete(int id)
        {
            if (id ==0)
            {
                id = int.Parse(Request.Form["idsupp"]);
            }
            try
            {
                ToyBusiness bu = new ToyBusiness();
                bu.DeleteToy(id);
            }
            catch
            {
                new Exception("Erreur de suppression");
            }
            return RedirectToAction("Index");
        }
    }
}
