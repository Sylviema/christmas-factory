﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using Fr.EQL.AI110.ChristmasFactory.Web.Models;

namespace Fr.EQL.AI110.ChristmasFactory.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        // routage :
        // http://localhost/home/privacy
        // http://localhost/controller>/<action>
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}