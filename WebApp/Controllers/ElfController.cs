﻿using Microsoft.AspNetCore.Mvc;
using Fr.EQL.AI110.ChristmasFactory.Business;
using Fr.EQL.AI110.ChristmasFactory.Entities;
using Fr.EQL.AI110.ChristmasFactory.Web.Models;

namespace Fr.EQL.AI110.ChristmasFactory.Web.Controllers
{
    public class ElfController : Controller
    {
        private IActionResult EditerElfe(string rubrique, Elf elf)
        {
            ViewBag.Rubrique = rubrique;
            return View("Edit", elf);
        }

        //par défaut : GET
        //attribut httpGet
        [HttpGet]
        public IActionResult Create()
        {
            ElfBusiness bu = new ElfBusiness();
            ElfCategoryBusiness elfCatBu = new ElfCategoryBusiness();
            ViewBag.Categories = elfCatBu.GetElfCategories();
            return EditerElfe("Création d'un elfe", null);

        }
        [HttpPost]
        public IActionResult Create(Elf elf)
        {
            ElfCategoryBusiness elfCatBu = new ElfCategoryBusiness();
            ViewBag.Categories = elfCatBu.GetElfCategories();

            if (ModelState.IsValid)
            {
                ElfBusiness bu = new ElfBusiness();
                if (elf.IdCategory == 0 )
                {
                    elf.IdCategory = 1;
                }
                bu.SaveElf(elf);
                return RedirectToAction("Index");
            }
            else
            {
                return EditerElfe("Création d'un elfe", elf);
            }

        }
        [HttpGet]
        public IActionResult Index()
        {
            ElfSearchViewModel model = new ElfSearchViewModel();

            ElfBusiness elfBu = new ElfBusiness();
            ElfCategoryBusiness elfCatBu = new ElfCategoryBusiness();
            model.Elves = elfBu.SearchElves("",null,"",0);
            model.CategoryNames = elfCatBu.GetElfCategories();
            return View(model);

            //try
            //{
            //ElfBusiness bu = new ElfBusiness();
            //List<Elf> elves = bu.GetElves();
            //return View(elves);
            //}
            //catch (Exception ex)
            //{
            //    ViewBag.Error = ex.Message;
            //    return View("Error");
            //}
            
        }
        [HttpPost]
        public IActionResult Index(ElfSearchViewModel model)
        {
            ElfBusiness elfBu = new ElfBusiness();
            model.Elves = elfBu.SearchElves(model.ElfName,model.IsAvailable,model.ElfPicture,model.IdCategory);
            ElfCategoryBusiness elfCatBu = new ElfCategoryBusiness();
            model.CategoryNames = elfCatBu.GetElfCategories();
            return View(model);
            
        }

        public IActionResult Infos(int id)
        {
            ElfBusiness bu = new ElfBusiness();
            Elf elf = bu.GetElfById(id);
            return View("Detail",elf);
        }

        [HttpGet]
        public IActionResult Update(int id)
        {
            ElfBusiness bu = new ElfBusiness();
            Elf elf = bu.GetElfById(id);
            ElfCategoryBusiness elfCatBu = new ElfCategoryBusiness();
            ViewBag.Categories = elfCatBu.GetElfCategories();
            return EditerElfe("Modification d'un elfe", elf);
        }
        [HttpPost]
        public IActionResult Update(Elf elf)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ElfBusiness bu = new ElfBusiness();
                    bu.SaveElf(elf);
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ViewBag.ErrorMessage = ex.Message;
                    return EditerElfe("Modification d'un elfe", elf);
                }
            }
            else
            {
                return EditerElfe("Modification d'un elfe", elf);
            }

        }

        [HttpPost]
        public IActionResult Delete(int id)
        {
            if (id == 0)
            {
                id = int.Parse(Request.Form["idsupp"]);
            }
            try
            {
                ElfBusiness bu = new ElfBusiness();
                bu.DeleteElf(id);
            }
            catch (Exception ex)
            {
                TempData.Add("Erreur", "Votre elfe ne peut être supprimé car il est en pleine fabrication de jouet!");
                return RedirectToAction("Index");
            }
            return View("Index");
        }

        #region QueryString
        public IActionResult Detail()
        {
            int id = int.Parse(Request.Query["id"]);
            ElfBusiness bu = new ElfBusiness();
            Elf elf = bu.GetElfById(id);
            return View(elf);
        }
        #endregion

        #region Create
        //ancienne version
        public IActionResult Nouveau()
        {
            if (Request.Method == "GET")
            {
                return View();
            }
            else
            {
                bool isAvailable = false;
                string name = Request.Form["name"];
                string img = Request.Form["img"];
                if (Request.Form["checkbox"] != "")
                {
                    isAvailable = true;
                }
                ElfBusiness bu = new ElfBusiness();
                Elf elf = new Elf(0,name,isAvailable,img);
                bu.SaveElf(elf);
                Response.ContentType = "text/html;charset=utf-8";
                return Content("Merci :)");
            }
        }
        #endregion
    }
}
