﻿using Microsoft.AspNetCore.Mvc;

namespace Fr.EQL.AI110.ChristmasFactory.Web.Controllers
{
    public class ChristmasController : Controller
    {
        public IActionResult Welcome()
        {
            return View();
        }

        public IActionResult Contact()
        {
            //Si ma requete est en get, je charge ma vue par défaut
            if (Request.Method == "GET")
            {
            return View();
            }
            //sinon j'affiche un message
            else
            {
                //Récupérer les données postéesS
                string email = Request.Form["email"];
                string message = Request.Form["message"];
                String[] args = new String[] { email, message};

                return View(args);
            }
        }

        //public IActionResult ExempleQS()
        //{
        //    // /christmas/exempleqs?word=hello&lang=fr*

        //    //Récupérer les paramètres
        //    string word = Request.Query["word"];
        //    string lang = Request.Query["lang"];
        //    int nb = int.Parse(Request.Query["nb"]);

        //    string traduction = "";

        //    for (int i = 0; i < nb; i++)
        //    {
        //        traduction += word + "<br/>";
        //    }
        //    Response.ContentType = "text/html";
        //    return Content(traduction);
        //}
    }
}
