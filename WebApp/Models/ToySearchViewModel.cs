﻿using Fr.EQL.AI110.ChristmasFactory.Entities;

namespace Fr.EQL.AI110.ChristmasFactory.Web.Models
{
    public class ToySearchViewModel
    {
        #region Critères de recherche
        public int IdResponsable { get; set; }
        public string ToyName { get; set; }
        public int MinPrice { get; set; }
        public int MaxPrice { get; set; }
        #endregion

        #region Liste de données pour affichage
        public List<Elf> Elves { get; set; }
        public List<ToyDetails> Toys { get; set; }
        #endregion
    }
}
