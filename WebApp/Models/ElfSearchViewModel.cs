﻿using Fr.EQL.AI110.ChristmasFactory.Entities;

namespace Fr.EQL.AI110.ChristmasFactory.Web.Models
{
    public class ElfSearchViewModel
    {
        public string ElfName { get; set; }
        public bool? IsAvailable    { get; set; }
        public string ElfPicture { get; set; }
        public int IdCategory { get; set; }
        public List<ElfCategory> CategoryNames { get; set; }
        
        public List<ElfDetails> Elves { get; set; }
    }
}
